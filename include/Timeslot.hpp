#pragma once
#include <iostream>
#include "Time.hpp"

struct Timeslot
{ 
    Timeslot();
    Timeslot(Time _begin, Time _end);
    
    Time begin;
    Time end;
    Minutes duration() const;
};

std::ostream& operator <<(std::ostream&, const Timeslot&);