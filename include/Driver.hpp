#pragma once
#include <iostream>
#include <vector>

#include "Timeslot.hpp"

using DriverId = unsigned;
using DriverIds = std::vector<DriverId>;
using Schedule = std::vector<Timeslot>;

struct Driver
{
    Driver(const Schedule&);
    DriverId driverId;
    Schedule shifts;
};
using Drivers = std::vector<Driver>;