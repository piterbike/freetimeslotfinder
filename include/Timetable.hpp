#pragma once
#include <unordered_set>

#include "Timeslot.hpp"
#include "Driver.hpp"

namespace
{
const Timeslot defaultWorkingHours{{8,00}, {17,00}};
}

using Index = unsigned;
using Timetable = std::vector<std::unordered_set<DriverId>>;
using Timeslots = std::vector<Timeslot>;

class DriversTimetable
{
public:
    DriversTimetable(const Drivers&, const Timeslot& workingHours = defaultWorkingHours);
    
    DriverIds getActiveDrivers() const;
    Timeslots findFreeTimeslots(const DriverIds&) const;
    Timeslots findFreeTimeslotsForAllDrivers() const;

private:
    Schedule getDriverBreaks(const Driver&) const;
    Index mapTimeToIndex(const Time&) const;
    void fillTimetable();
    
    const Drivers drivers;
    const Timeslot workingHours;
    Timetable timetable;
};
