#pragma once
#include <iostream>

using Hours = unsigned;
using Minutes = unsigned;

class Time
{
public:
    Time() = default;
    Time(Hours, Minutes);
    Time(Minutes);

    Hours getHours() const;
    Minutes getMinutes() const;
    Minutes inMinutes() const;

    Time operator+(const Time& other) const;
    Time operator-(const Time& other) const;
    bool operator==(const Time& other) const;
    bool operator<(const Time& other) const;
    bool operator<=(const Time& other) const;
    bool operator>(const Time& other) const;
    bool operator>=(const Time& other) const;

private:
    Minutes timeInMinutes;
};

std::ostream& operator <<(std::ostream&, const Time&);
