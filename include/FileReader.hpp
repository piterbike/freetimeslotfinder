#pragma once
#include <fstream>

#include "Time.hpp"
#include "Driver.hpp"

class FileReader
{
public:
    FileReader(const char*);
    ~FileReader();
    Drivers readDrivers();
private:
    Time parseTime(const std::string&) const;

    std::ifstream infile;
};