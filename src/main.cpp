#include <iostream>
#include <exception>
#include "FileReader.hpp"
#include "Timetable.hpp"

int main(int argc, char *argv[])
{
    try
    {
        if(argc < 2)
        {
            throw std::invalid_argument("Missing file path!");
        }
        FileReader fileReader(argv[1]);
        DriversTimetable drivers(fileReader.readDrivers());

        const auto freeTimeslots = drivers.findFreeTimeslotsForAllDrivers();
        for(auto& slot : freeTimeslots)
        {
            std::cout << slot << '\n';
        }
    }
    catch(const std::exception& exc)
    {
        std::cout << exc.what() << '\n';
    }

    return 0;
}