#include <iostream>
#include <sstream>

#include "FileReader.hpp"

FileReader::FileReader(const char* filePath) : infile(filePath)
{
    if(not infile.is_open()) 
    {
        throw std::invalid_argument("Cannot open the file");
    }
}

FileReader::~FileReader()
{
    if(infile.is_open())
    {
        infile.close();
    }
}

Time FileReader::parseTime(const std::string& time) const
{
    std::stringstream ss;
    Hours hh;
    Minutes mm;
    char colon;

    ss << time;
    ss >> hh >> colon >> mm;
    
    if(colon != ':')
    {
        throw std::invalid_argument("Incorrect time format");
    }

    return Time{hh, mm};
}

Drivers FileReader::readDrivers()
{
    Drivers drivers;
    size_t numberOfDrivers, numberOfShifts;
    infile >> numberOfDrivers;

    while(numberOfDrivers-- > 0)
    {
        infile >> numberOfShifts;
        Schedule driverShifts;
        driverShifts.reserve(numberOfShifts);
        std::string shiftBegin, shiftEnd;

        while(numberOfShifts-- > 0)
        {
            infile >> shiftBegin >> shiftEnd;
            driverShifts.emplace_back(parseTime(shiftBegin).inMinutes(), parseTime(shiftEnd).inMinutes());
        }
        drivers.emplace_back(driverShifts);
    }
    return drivers;
}