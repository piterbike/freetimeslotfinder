#include <iostream>
#include <iomanip>
#include <cassert>

#include "Time.hpp"

Time::Time(Minutes mm) : timeInMinutes(mm) {};

Time::Time(Hours hh, Minutes mm)
{
    timeInMinutes = (hh * 60) + mm;
};

Minutes Time::inMinutes() const { return timeInMinutes; }

Hours Time::getHours() const { return timeInMinutes / 60; }
Minutes Time::getMinutes() const { return timeInMinutes % 60; }

Time Time::operator+(const Time& other) const { return timeInMinutes + other.timeInMinutes; }
Time Time::operator-(const Time& other) const { return timeInMinutes - other.timeInMinutes; }
bool Time::operator==(const Time& other) const { return timeInMinutes == other.timeInMinutes; }
bool Time::operator<(const Time& other) const { return timeInMinutes < other.timeInMinutes; }
bool Time::operator<=(const Time& other) const { return timeInMinutes <= other.timeInMinutes; }
bool Time::operator>(const Time& other) const { return timeInMinutes > other.timeInMinutes; }
bool Time::operator>=(const Time& other) const { return timeInMinutes <= other.timeInMinutes; }

std::ostream& operator <<(std::ostream& out, const Time& time)
{
    out << std::setw(2) << std::setfill('0') << time.getHours() << ":" \
        << std::setw(2) << std::setfill('0') << time.getMinutes();
    return out;
}
