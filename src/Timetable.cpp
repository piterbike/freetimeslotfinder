#include <algorithm>

#include "Timetable.hpp"

DriversTimetable::DriversTimetable(const Drivers& drivers, const Timeslot& workingHours) : 
    drivers{drivers}, workingHours{workingHours} 
{
    fillTimetable();
}

Schedule DriversTimetable::getDriverBreaks(const Driver& driver) const
{
    if(driver.shifts.empty())
    {
        std::cout << "Driver with id:" << driver.driverId << " has no shifts!";
        return {};
    }
    
    Schedule breaksSchedule;

    const auto& firstShift = driver.shifts.front();
    if(workingHours.begin < firstShift.begin)
    {
        breaksSchedule.emplace_back(workingHours.begin, firstShift.begin);
    }
    
    for(size_t i = 0; i < driver.shifts.size() - 1; ++i)
    {
        Timeslot maybeBreak{driver.shifts[i].end, driver.shifts[i+1].begin};
        if(maybeBreak.duration() > 0)
        {
            breaksSchedule.emplace_back(maybeBreak);    
        } 
    }

    const auto& lastShift = driver.shifts.back();
    if(lastShift.end < workingHours.end)
    {
        breaksSchedule.emplace_back(lastShift.end, workingHours.end);
    }
    
    return breaksSchedule;
}


Index DriversTimetable::mapTimeToIndex(const Time& time) const
{
    return time.inMinutes() - workingHours.begin.inMinutes();
}

void DriversTimetable::fillTimetable()
{
    const auto workdayDurationInMinutes = workingHours.duration();
    timetable = Timetable(workdayDurationInMinutes+1);

    for(const auto& driver : drivers)
    {
        const auto& breaks = getDriverBreaks(driver);
        for(const auto& b : breaks)
        {
            auto begin = mapTimeToIndex(b.begin);
            auto end = mapTimeToIndex(b.end);
            for(auto timeIndex = begin; timeIndex <= end; ++timeIndex)
            {
                timetable[timeIndex].insert(driver.driverId);
            }
        }
    }
}

DriverIds DriversTimetable::getActiveDrivers() const 
{ 
    DriverIds driverIds;
    std::transform(drivers.begin(), drivers.end(), std::back_inserter(driverIds), [](const auto& driver){
        return driver.driverId;
    });
    return driverIds; 
}

Timeslots DriversTimetable::findFreeTimeslots(const DriverIds& ids) const
{
    Timeslots freeSlots;
    const auto containsDriverIds = [&ids](const std::unordered_set<DriverId>& driverSet){
        return std::all_of(ids.begin(), ids.end(), [&driverSet](auto id){
            return driverSet.find(id) != driverSet.end();
        });
    };
    auto timeStart = 0u;
    auto timeEnd = timeStart;

    bool isLastMatching = true;
    for(auto idx = timetable.begin(); idx < timetable.end(); ++idx)
    {
        if((containsDriverIds(*idx)))
        {
            if(not isLastMatching)
            {
                timeStart = std::distance(timetable.begin(), idx);
            }
            timeEnd = std::distance(timetable.begin(), idx);
            isLastMatching = true;
            continue;
        }
        if(isLastMatching)
        {
            Timeslot freeSlot{workingHours.begin + timeStart, workingHours.begin + timeEnd};
            if(freeSlot.duration() > 0)
            {
                freeSlots.push_back(freeSlot);
            }
        }
        isLastMatching = false;
    }
    return freeSlots;
}

Timeslots DriversTimetable::findFreeTimeslotsForAllDrivers() const 
{
    return findFreeTimeslots(getActiveDrivers());
}