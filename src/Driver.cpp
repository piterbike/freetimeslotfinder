#include "Driver.hpp"

namespace 
{
struct IdGenerator
{
    static DriverId getNewId() 
    { 
        static DriverId id{0};
        return id++; 
    }
};
}

Driver::Driver(const Schedule& shifts) : shifts{shifts}, driverId{IdGenerator::getNewId()}
{
}