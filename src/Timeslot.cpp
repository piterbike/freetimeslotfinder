#include "Timeslot.hpp"

Timeslot::Timeslot() = default;
Timeslot::Timeslot(Time _begin, Time _end) : begin{_begin}, end{_end} {}

Minutes Timeslot::duration() const { return end.inMinutes() - begin.inMinutes(); }

std::ostream& operator <<(std::ostream& out, const Timeslot& timeslot)
{
    out << timeslot.begin << " - " << timeslot.end;
    return out;
}